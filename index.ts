import maplibregl, { Map, NavigationControl } from 'maplibre-gl';
import 'maplibre-gl/dist/maplibre-gl.css';
import { MaplibreExportControl, Size, PageOrientation, Format, DPI } from './src/lib/index';
import './src/scss/maplibre-gl-export.scss';
import { Protocol } from 'pmtiles';

//////////////////// FUNCTIONS
async function loadCustomImages(map, customImages) {
	// customImages contient une liste d'objets contenant le nom du sprite et l'url
	customImages.forEach(async (marker) => {
		const image = await map.loadImage(marker.url);
		map.addImage(marker.name, image.data);
	});
}

async function getPosts(postType) {
	const response = await fetch(
		`https://darkred-sparrow-412561.hostingersite.com/wp-json/wp/v2/${postType}?per_page=100`
	); // If there were more than 100 posts, it would be necessary to implement a loop through all pages
	const posts = await response.json();

	return posts;
}

async function getCategories() {
	const response = await fetch(
		`https://darkred-sparrow-412561.hostingersite.com/wp-json/wp/v2/categories`
	);
	const categories = await response.json();
	return categories;
}

async function getMedia(id) {
	const response = await fetch(
		`https://darkred-sparrow-412561.hostingersite.com/wp-json/wp/v2/media/${id}`
	);
	const media = await response.json();
	return media.source_url;
}

// Convert posts to geoJSON
function posts2geojson(postType, geojsonObject) {
	getPosts(postType).then((posts) => {
		posts.forEach((post) => {
			geojsonObject.features.push({
				type: 'Feature',
				geometry: {
					type: 'Point',
					coordinates: [post.acf.longitude, post.acf.latitude]
				},
				properties: {
					title: post.title.rendered,
					index: `${post.acf.index}\n`,
					marker: post.acf.viewpoint ? 'pointeur-oeil-vert' : 'pointest-ND-vert',
					markerNoir: post.acf.viewpoint ? 'pointeur-oeil-noir' : 'pointest-ND-noir',
					textOffset: post.acf.viewpoint ? [0, 0] : [0, -0.7]
				}
			});
		});
	});
}

// Convert gpx path to geoJSON
function path2geojson(path_file, geojsonObject) {
	getMedia(path_file).then((gpxFile) => {
		// Convert gpx to geoJSON for pathing
		fetch(gpxFile)
			.then((response) => response.text())
			.then((text) => {
				const parser = new DOMParser();
				const doc = parser.parseFromString(text, 'application/xml');
				const els = doc.getElementsByTagName('trkpt');

				const path = {
					type: 'Feature',
					geometry: {
						type: 'LineString',
						properties: {},
						coordinates: []
					}
				};

				for (const pt of els) {
					path.geometry.coordinates.push([pt.getAttribute('lon'), pt.getAttribute('lat')]);
				}

				geojsonObject.features.push(path);
			});
	});
}

// Gets the path for each category (== tour)
getCategories().then((categories) => {
	categories.forEach((category) => {
		// Primary path
		if (category.acf.tour) {
			path2geojson(category.acf.tour, paths);
		}

		// Secondary path
		if (category.acf.tour_secondary) {
			path2geojson(category.acf.tour_secondary, paths_secondary);
		}
	});
});

//////////////////// DEFINITIONS
const themeURL =
	'https://darkred-sparrow-412561.hostingersite.com/wp-content/themes/bma-tour-guide/assets';
const themeURL2 = 'http://localhost:5173/';

const map = (window.map = new maplibregl.Map({
	container: 'map',
	zoom: 12,
	center: [4.34037, 50.82786],
	hash: true,
	// style: `${themeURL}/bma-fond-carte-WIP-rose.json`,
	style: `${themeURL2}/bma-test.json`,
	maxZoom: 20,
	maxPitch: 85
}));

// Prepare geojson objects to add them as layers later
const projets = {
	type: 'FeatureCollection',
	features: []
};

const nonDetaille = {
	type: 'FeatureCollection',
	features: []
};

const eatDrink = {
	type: 'FeatureCollection',
	features: []
};

const paths = {
	type: 'FeatureCollection',
	features: []
};

const paths_secondary = {
	type: 'FeatureCollection',
	features: []
};

//////////////////// PREPARATION
posts2geojson('posts', projets);
posts2geojson('non-detaille', nonDetaille);

getPosts('eat-drink').then((posts) => {
	const catsCounter = {};

	posts.forEach((post) => {
		if (catsCounter[post.categories[0]]) {
			catsCounter[post.categories[0]] += 1;
		} else {
			catsCounter[post.categories[0]] = 1;
		}

		eatDrink.features.push({
			type: 'Feature',
			geometry: {
				type: 'Point',
				coordinates: [post.acf.longitude, post.acf.latitude]
			},
			properties: {
				index: `${catsCounter[post.categories[0]]}`
			}
		});
	});
});

// Adds compass and zoom controls
map.addControl(
	new maplibregl.NavigationControl({
		visualizePitch: true,
		showZoom: true,
		showCompass: true
	})
);

// Adds controls to export the map as an image
map.addControl(
	new MaplibreExportControl({
		PageSize: Size.A4,
		PageOrientation: PageOrientation.Portrait,
		Format: Format.PNG,
		DPI: DPI[500],
		Crossha5r: false,
		PrintableArea: true,
		Local: 'en'
	}),
	'top-right'
);

map.on('load', async () => {
	// This is the important part of this example: the addLayer
	// method takes 2 arguments: the layer as an object, and a string
	// representing another layer's name. if the other layer
	// exists in the stylesheet already, the new layer will be positioned
	// right before that layer in the stack, making it possible to put
	// 'overlays' anywhere in the layer stack.
	// Insert the layer beneath the first symbol layer.

	// Adding layers in the order of importance : least important layer first, most important layer last

	// Add a line layer for secondary paths
	map.addSource('paths_secondary', {
		type: 'geojson',
		data: paths_secondary
	});

	map.addLayer({
		id: 'paths_secondary-lines',
		type: 'line',
		source: 'paths_secondary',
		layout: {
			'line-join': 'round',
			'line-cap': 'round',
			visibility: 'visible'
		},
		paint: {
			'line-color': '#004c5d',
			'line-dasharray': [1, 2],
			'line-width': 5,
			'line-opacity': 1
		}
	});

	// Add a line layer for primary paths
	map.addSource('paths', {
		type: 'geojson',
		data: paths
	});

	map.addLayer({
		id: 'paths-lines',
		type: 'line',
		source: 'paths',
		layout: {
			'line-join': 'round',
			'line-cap': 'round',
			visibility: 'visible'
		},
		paint: {
			'line-color': '#004c5d',
			'line-width': 5,
			'line-opacity': 1
		}
	});

	// Add a symbol layer for eat-drink (displaying text only)
	map.addSource('eat-drink', {
		type: 'geojson',
		data: eatDrink
	});

	map.addLayer({
		id: 'eat-drink-markers',
		type: 'symbol',
		source: 'eat-drink',
		maxzoom: 18,
		minzoom: 13,
		layout: {
			'icon-allow-overlap': true,
			'icon-overlap': 'always',
			'icon-image': 'pointeur-BM-vert',
			'icon-size': 2.4,
			'text-field': ['get', 'index'],
			'text-font': ['quicksand-bold'],
			'text-anchor': 'center',
			'text-size': 13
		},
		paint: {
			'icon-color': 'white',
			'icon-halo-color': '#004c5d',
			'icon-halo-width': 3,
			'text-color': '#004c5d'
		}
	});

	// Add a symbol layer for non-detaille
	map.addSource('non-detaille', {
		type: 'geojson',
		data: nonDetaille,
		cluster: true,
		clusterMaxZoom: 15, // Max zoom to cluster points on
		clusterRadius: 30 // Radius of each cluster when clustering points (defaults to 50)
	});

	map.addLayer({
		id: 'non-detaille-markers',
		type: 'symbol',
		source: 'non-detaille',
		layout: {
			'icon-allow-overlap': true,
			'icon-overlap': 'always',
			'icon-image': ['get', 'marker'],
			'icon-size': 3.3,
			'text-size': 11,
			'text-field': ['get', 'index'],
			'text-font': ['quicksand-bold'],
			'text-offset': ['get', 'textOffset'],
			'text-anchor': 'center'
		},
		paint: {
			'icon-color': 'white',
			'icon-halo-color': '#004c5d',
			'icon-halo-width': 3,
			'text-color': '#004c5d'
		}
	});

	// Add a symbol layer for projects
	map.addSource('projets', {
		type: 'geojson',
		data: projets,
		cluster: true,
		clusterMaxZoom: 14, // Max zoom to cluster points on
		clusterRadius: 30, // Radius of each cluster when clustering points (defaults to 50)
		clusterProperties: {
			textSum: [
				['concat', ['accumulated'], ['get', 'textSum']],
				['get', 'index']
			]
		}
	});

	map.addLayer({
		id: 'projets-markers-unclustered',
		type: 'symbol',
		source: 'projets',
		filter: ['!', ['has', 'point_count']],
		layout: {
			'icon-allow-overlap': true,
			'text-allow-overlap': true,
			'icon-ignore-placement': true,
			'icon-overlap': 'always',
			'icon-image': 'pointest-vert',
			'icon-size': 5,
			'text-field': ['get', 'index'],
			'text-font': ['quicksand-bold'],
			'text-offset': [0, -0.8],
			'text-anchor': 'center'
		},
		paint: {
			'icon-color': '#004c5d',
			'text-color': 'white'
		}
	});

	map.addLayer({
		id: 'projets-markers-clustered',
		type: 'symbol',
		source: 'projets',
		filter: ['all', ['has', 'point_count'], ['>', 8, ['get', 'point_count']]],
		layout: {
			'icon-allow-overlap': true,
			'text-allow-overlap': true,
			'icon-ignore-placement': true,
			'icon-overlap': 'always',
			'icon-image': 'pointest-vert-stretch',
			'icon-text-fit': 'both',
			'icon-text-fit-padding': [10, 8, 10, 8],
			'text-field': ['get', 'textSum'],
			'text-font': ['quicksand-bold'],
			'text-anchor': 'bottom',
			'icon-anchor': 'bottom'
		},
		paint: {
			'text-color': 'white'
		}
	});

	map.addLayer({
		id: 'projets-markers-clustered-far',
		type: 'symbol',
		source: 'projets',
		filter: ['all', ['has', 'point_count'], ['<', 8, ['get', 'point_count']]],
		layout: {
			'icon-allow-overlap': true,
			'text-allow-overlap': true,
			'icon-ignore-placement': true,
			'icon-overlap': 'always',
			'icon-image': 'pointest-vert-stretch',
			'icon-text-fit': 'both',
			'icon-text-fit-padding': [10, 11, 20, 11],
			'text-field': ['concat', ['get', 'point_count'], ['literal', 'p']],
			'text-font': ['quicksand-bold'],
			'text-anchor': 'bottom',
			'icon-anchor': 'bottom'
		},
		paint: {
			'text-color': 'white'
		}
	});
});

// //////////////////// INTERFACE
const printButton = document.querySelector('.maplibregl-ctrl-icon.maplibregl-export-control');

// Le chemin est toujours '../../bma-fond-carte-noir.json'

printButton.addEventListener('click', () => {
	// calculate scale
	// source: https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Resolution_and_Scale

	document.querySelector('button.generate-button').addEventListener('click', () => {
		// 	repaint added layers in black
		map.setPaintProperty('paths-lines', 'line-color', 'black');
		map.setPaintProperty('paths_secondary-lines', 'line-color', 'black');

		map.setLayoutProperty('eat-drink-markers', 'icon-image', 'pointeur-BM-noir');
		map.setPaintProperty('eat-drink-markers', 'text-color', 'black');

		map.setLayoutProperty('non-detaille-markers', 'icon-image', ['get', 'markerNoir']);
		map.setPaintProperty('non-detaille-markers', 'text-color', 'black');

		map.setLayoutProperty('projets-markers-unclustered', 'icon-image', 'pointest-noir');
		map.setLayoutProperty('projets-markers-clustered', 'icon-image', 'pointest-noir-stretch');
		map.setLayoutProperty('projets-markers-clustered-far', 'icon-image', 'pointest-noir-stretch');

		// 	hide layers
		// (might optimize later)
		map.setLayoutProperty('landcover_grass_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_wood_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_cemetery_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_cemetery_pattern', 'visibility', 'none');
		map.setLayoutProperty('water', 'visibility', 'none');
		map.setLayoutProperty('waterway', 'visibility', 'none');
		map.setLayoutProperty('rail', 'visibility', 'none');
		map.setLayoutProperty('rail_hatch', 'visibility', 'none');
		map.setLayoutProperty('road_area_bridge', 'visibility', 'none');
		map.setLayoutProperty('road_area_pier', 'visibility', 'none');
		map.setLayoutProperty('road_pier', 'visibility', 'none');
		map.setLayoutProperty('road_path', 'visibility', 'none');
		map.setLayoutProperty('road_secondary', 'visibility', 'none');
		map.setLayoutProperty('road_primary', 'visibility', 'none');
		map.setLayoutProperty('road_highway_casing', 'visibility', 'none');
		map.setLayoutProperty('road_highway', 'visibility', 'none');
		map.setLayoutProperty('building_fill', 'visibility', 'none');
		map.setLayoutProperty('building_pattern', 'visibility', 'none');
		map.setLayoutProperty('boundary_state', 'visibility', 'none');
		map.setLayoutProperty('boundary_country_z5-', 'visibility', 'none');
		map.setLayoutProperty('boundary_country_z0-4', 'visibility', 'none');
		map.setLayoutProperty('water_name_lakeline', 'visibility', 'none');
		map.setLayoutProperty('water_name_way', 'visibility', 'none');
		map.setLayoutProperty('water_name_sea', 'visibility', 'none');
		map.setLayoutProperty('water_name_ocean', 'visibility', 'none');
		map.setLayoutProperty('road_label_primary', 'visibility', 'none');
		map.setLayoutProperty('road_label_secondary', 'visibility', 'none');
		map.setLayoutProperty('place_label_park', 'visibility', 'none');
		map.setLayoutProperty('place_label_village', 'visibility', 'none');
		map.setLayoutProperty('place_label_city', 'visibility', 'none');
		map.setLayoutProperty('place_label_town', 'visibility', 'none');
		map.setLayoutProperty('place_state-label', 'visibility', 'none');
		map.setLayoutProperty('place_label_country', 'visibility', 'none');
		map.setLayoutProperty('place_label_continent', 'visibility', 'none');
		map.setLayoutProperty('Station', 'visibility', 'none');
		map.setLayoutProperty('background', 'visibility', 'none');
		map.setLayoutProperty('water', 'visibility', 'none');
		map.setLayoutProperty('landcover_grass_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_grass_pattern', 'visibility', 'none');
		map.setLayoutProperty('landcover_wood_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_wood_pattern', 'visibility', 'none');
		map.setLayoutProperty('landcover_cemetery_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_cemetery_pattern', 'visibility', 'none');
		map.setLayoutProperty('waterway', 'visibility', 'none');
		map.setLayoutProperty('rail', 'visibility', 'none');
		map.setLayoutProperty('rail_hatch', 'visibility', 'none');
		map.setLayoutProperty('road_area_bridge', 'visibility', 'none');
		map.setLayoutProperty('road_area_pier', 'visibility', 'none');
		map.setLayoutProperty('road_pier', 'visibility', 'none');
		map.setLayoutProperty('road_path', 'visibility', 'none');
		map.setLayoutProperty('road_secondary', 'visibility', 'none');
		map.setLayoutProperty('road_primary', 'visibility', 'none');
		map.setLayoutProperty('road_highway_casing', 'visibility', 'none');
		map.setLayoutProperty('road_highway', 'visibility', 'none');
		map.setLayoutProperty('building_fill', 'visibility', 'none');
		map.setLayoutProperty('building_pattern', 'visibility', 'none');
		map.setLayoutProperty('boundary_state', 'visibility', 'none');
		map.setLayoutProperty('boundary_country_z5-', 'visibility', 'none');
		map.setLayoutProperty('boundary_country_z0-4', 'visibility', 'none');
		map.setLayoutProperty('water_name_lakeline', 'visibility', 'none');
		map.setLayoutProperty('water_name_way', 'visibility', 'none');
		map.setLayoutProperty('water_name_sea', 'visibility', 'none');
		map.setLayoutProperty('water_name_ocean', 'visibility', 'none');
		map.setLayoutProperty('road_label_primary', 'visibility', 'none');
		map.setLayoutProperty('road_label_secondary', 'visibility', 'none');
		map.setLayoutProperty('Station', 'visibility', 'none');
		map.setLayoutProperty('place_label_park', 'visibility', 'none');
		map.setLayoutProperty('place_label_village', 'visibility', 'none');
		map.setLayoutProperty('place_label_city', 'visibility', 'none');
		map.setLayoutProperty('place_label_town', 'visibility', 'none');
		map.setLayoutProperty('place_state-label', 'visibility', 'none');
		map.setLayoutProperty('place_label_country', 'visibility', 'none');
		map.setLayoutProperty('place_label_continent', 'visibility', 'none');
		map.setLayoutProperty('background', 'visibility', 'none');
		map.setLayoutProperty('landcover_grass_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_grass_pattern', 'visibility', 'none');
		map.setLayoutProperty('landcover_wood_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_wood_pattern', 'visibility', 'none');
		map.setLayoutProperty('landcover_cemetery_fill', 'visibility', 'none');
		map.setLayoutProperty('water', 'visibility', 'none');
		map.setLayoutProperty('waterway', 'visibility', 'none');
		map.setLayoutProperty('rail', 'visibility', 'none');
		map.setLayoutProperty('rail_hatch', 'visibility', 'none');
		map.setLayoutProperty('road_area_bridge', 'visibility', 'none');
		map.setLayoutProperty('road_area_pier', 'visibility', 'none');
		map.setLayoutProperty('road_pier', 'visibility', 'none');
		map.setLayoutProperty('road_path', 'visibility', 'none');
		map.setLayoutProperty('road_secondary', 'visibility', 'none');
		map.setLayoutProperty('road_primary', 'visibility', 'none');
		map.setLayoutProperty('road_highway_casing', 'visibility', 'none');
		map.setLayoutProperty('road_highway', 'visibility', 'none');
		map.setLayoutProperty('building_fill', 'visibility', 'none');
		map.setLayoutProperty('building_pattern', 'visibility', 'none');
		map.setLayoutProperty('boundary_state', 'visibility', 'none');
		map.setLayoutProperty('boundary_country_z5-', 'visibility', 'none');
		map.setLayoutProperty('boundary_country_z0-4', 'visibility', 'none');
		map.setLayoutProperty('water_name_lakeline', 'visibility', 'none');
		map.setLayoutProperty('water_name_way', 'visibility', 'none');
		map.setLayoutProperty('water_name_sea', 'visibility', 'none');
		map.setLayoutProperty('water_name_ocean', 'visibility', 'none');
		map.setLayoutProperty('road_label_primary', 'visibility', 'none');
		map.setLayoutProperty('road_label_secondary', 'visibility', 'none');
		map.setLayoutProperty('place_label_park', 'visibility', 'none');
		map.setLayoutProperty('place_label_village', 'visibility', 'none');
		map.setLayoutProperty('place_label_city', 'visibility', 'none');
		map.setLayoutProperty('place_label_town', 'visibility', 'none');
		map.setLayoutProperty('place_state-label', 'visibility', 'none');
		map.setLayoutProperty('place_label_country', 'visibility', 'none');
		map.setLayoutProperty('place_label_continent', 'visibility', 'none');
		map.setLayoutProperty('background', 'visibility', 'none');
		map.setLayoutProperty('landcover_grass_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_grass_pattern', 'visibility', 'none');
		map.setLayoutProperty('landcover_wood_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_wood_pattern', 'visibility', 'none');
		map.setLayoutProperty('landcover_cemetery_fill', 'visibility', 'none');
		map.setLayoutProperty('landcover_cemetery_pattern', 'visibility', 'none');
		map.setLayoutProperty('water', 'visibility', 'none');
		map.setLayoutProperty('waterway', 'visibility', 'none');
		map.setLayoutProperty('rail', 'visibility', 'none');
		map.setLayoutProperty('rail_hatch', 'visibility', 'none');
		map.setLayoutProperty('road_area_bridge', 'visibility', 'none');
		map.setLayoutProperty('road_area_pier', 'visibility', 'none');
		map.setLayoutProperty('road_pier', 'visibility', 'none');
		map.setLayoutProperty('road_path', 'visibility', 'none');
		map.setLayoutProperty('road_secondary', 'visibility', 'none');
		map.setLayoutProperty('road_primary', 'visibility', 'none');
		map.setLayoutProperty('road_highway_casing', 'visibility', 'none');
		map.setLayoutProperty('road_highway', 'visibility', 'none');
		map.setLayoutProperty('building_fill', 'visibility', 'none');
		map.setLayoutProperty('building_pattern', 'visibility', 'none');
		map.setLayoutProperty('boundary_state', 'visibility', 'none');
		map.setLayoutProperty('boundary_country_z5-', 'visibility', 'none');
		map.setLayoutProperty('boundary_country_z0-4', 'visibility', 'none');
		map.setLayoutProperty('water_name_lakeline', 'visibility', 'none');
		map.setLayoutProperty('water_name_way', 'visibility', 'none');
		map.setLayoutProperty('water_name_sea', 'visibility', 'none');
		map.setLayoutProperty('water_name_ocean', 'visibility', 'none');
		map.setLayoutProperty('road_label_primary', 'visibility', 'none');
		map.setLayoutProperty('road_label_secondary', 'visibility', 'none');
		map.setLayoutProperty('place_label_park', 'visibility', 'none');
		map.setLayoutProperty('place_label_village', 'visibility', 'none');
		map.setLayoutProperty('place_label_city', 'visibility', 'none');
		map.setLayoutProperty('place_label_town', 'visibility', 'none');
		map.setLayoutProperty('place_state-label', 'visibility', 'none');
		map.setLayoutProperty('place_label_country', 'visibility', 'none');
		map.setLayoutProperty('place_label_continent', 'visibility', 'none');
		map.setLayoutProperty('Station', 'visibility', 'none');
	});
});

// TESTING
document.getElementById('zoomto').addEventListener('click', () => {
	// Geographic coordinates of the LineString
	const coordinates = paths.features[0].geometry.coordinates;

	// Pass the first coordinates in the LineString to `lngLatBounds` &
	// wrap each coordinate pair in `extend` to include them in the bounds
	// result. A variation of this technique could be applied to zooming
	// to the bounds of multiple Points or Polygon geomteries - it just
	// requires wrapping all the coordinates with the extend method.
	const bounds = coordinates.reduce(
		(bounds, coord) => {
			return bounds.extend(coord);
		},
		new maplibregl.LngLatBounds(coordinates[0], coordinates[0])
	);

	map.fitBounds(bounds, {
		padding: 20
	});
});
