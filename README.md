# maplibre-gl-export-fork

## Introduction

It's just a small fork of [this
project](https://github.com/watergis/maplibre-gl-export). Credits to
[watergis](https://github.com/watergis).

I didn't want to fork the whole monorepo + it's such a specific modification
I didn't think it could deserve a pull request + my code is messy + I didn't
want to use Github :)

## Changelog

+ Added ability to load custom images so that they're loaded in the image
generation
+ (TODO) Add the ability to generate 2 images in black and white with one click
  of a button, with the intention to print them as spot colors
